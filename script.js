//------------------- RAINBOW ---------------

// Déplace rainbow de gauche à droite
anime({
    targets: rainbow,
    opacity: 1,
    translateX: 3000,
    duration: 3500,
    delay: 500,
    direction: 'alternate',
    easing: 'linear',
    complete: rainbowVertical
})


// Déplace et tourne rainbow
function rainbowVertical() {
    anime({
        targets: rainbow,
        keyframes: [
            {opacity: 0, duration: 100},
            {translateX: 1075, translateY: -650, easing: 'linear', rotate: 85, duration: 100, opacity: 0},
            {translateX: 1075, translateY: -650, easing: 'linear', rotate: 85, duration: 1466, opacity: 1},
            {translateX: 1575, translateY: -650, easing: 'spring', rotate: 95, duration: 1666, opacity: 1},
            {translateX: 1325, translateY: -650, easing: 'linear', rotate: 90, opacity: 0, duration: 2000}
        ],
        delay: 3000
    })
}

// --------------- ÉLÉMENT PERSONNAGE ---------------

// Déplace robot dans l'écran
anime({
    targets: robot,
    keyframes: [
        {translateY: -768, duration: 3000},
        {translateX: -99, translateY: -788, easing: 'spring', rotate: -4, duration: 1666},
        {translateX: 99, translateY: -788, easing: 'spring', rotate: 4, duration: 1666},
        {translateX: 0, translateY: -768, easing: 'linear', rotate: 0, duration: 2000}
    ],
    delay: 8000
})

// déplace boîte dans l'écran
anime({
    targets: '.boite',
    keyframes: [
        {translateY: -768, duration: 3000},
        {translateX: -200, translateY: -788, easing: 'spring', rotate: -4, duration: 1666},
        {translateX: 200, translateY: -788, easing: 'spring', rotate: 4, duration: 1666},
        {translateX: 0, translateY: -769, easing: 'linear', rotate: 0, duration: 2000}
    ],
    delay: 8000,
    complete: bougerBoite
})

// Pousse boîte à côté du texte, ramène boîte au milieu
function bougerBoite() {
    anime({
        targets: '.boite',
        translateX: 300,
        translateY: -900, 
        duration: 1000,  
        easing: 'easeInOutElastic',      
        delay: 1000,
        endDelay: 3300,
        direction: 'alternate'
    })
}

anime({
    targets: yeux,
    keyframes: [
        {translateX: 0, translateY: 0, duration: 1000},
        {translateX: -30, translateY: -25, easing: 'steps(8)'},
        {translateX: -30, translateY: -25, easing: 'steps(8)'},
        {translateX: 30, translateY: -25, easing: 'steps(8)'},
        {translateX: 30, translateY: -25, easing: 'steps(8)'},
        {translateX: 0, translateY: 0, easing: 'steps(8)'},
    ],
    duration: 6000,
    delay: 9000
})

// Bouge bras droit
anime({
    targets: brasD,
    keyframes: [
        {easing: 'spring', rotate: 20},
        {easing: 'spring', rotate: -20},
        {easing: 'linear', rotate: 0, duration: 2000}
    ],
    duration: 5000,
    delay: 11000
})

// Bouge bras gauche
anime({
    targets: brasG,
    keyframes: [
        {easing: 'spring', rotate: 20},
        {easing: 'spring', rotate: -20},
        {easing: 'linear', rotate: 0, duration: 2000}
    ],
    duration: 5000,
    delay: 11000
})

// Ouvre + ferme panneau droit
anime({
    targets: lidD,
    keyframes: [
        {rotate: '230deg', delay: 9000, duration: 1000, easing: 'spring'},
        {rotate: '230deg', duration: 5500},
        {rotate: '0deg', duration: 1200, easing: 'easeOutBounce'},
    ],
})

// Ouvre + ferme panneau gauche
anime({
    targets: lidG,
    keyframes: [
        {rotate: '-240deg', delay: 9200, duration: 1000, easing: 'spring'},
        {rotate: '-240deg', duration: 5300},
        {rotate: '0deg', duration: 900, easing: 'easeOutBounce'},
    ],
})


// --------------------TEXTES ----------------------


var items = document.querySelectorAll('.texte .item');

// Anime la liste 
anime({
  targets: items,
  translateX: 800,
  delay: anime.stagger(600, {start: 18000})
});

// Anime le slogan
anime({
    targets: conclusion,
    translateY: 1200,
    delay: 25000,
    easing: 'easeOutQuart'
});

// Anime le div avec background blanc
anime({
    targets: white,
    translateX: 1200,
    easing: 'easeOutQuart',
    delay: 17000
})

// ----------------------OBJETS --------------------------

// Fait apparaître objets 1, 2, 3
anime({
    targets: '.objets .objetA',
    translateY: 250,
    translateX: 
        function(objetA, i) {
            return objetA.getAttribute('data-x') + (350 * i);
        },
    duration: 500,
    complete: bougerObjetsA
})

// Fait sortir objets 1, 2, 3
function bougerObjetsA() {
    anime({
        targets: '.objets .objetA',
        translateX: 1020,
        rotate: 360,
        duration: 2000,
        easing: 'easeInOutElastic',
        delay: anime.stagger(600),
        complete: placerObjetsB
    })
}

// Fait apparaître objets 4, 5, 6
function placerObjetsB() {
    anime({
        targets: '.objets .objetB',
        translateY: 250,
        translateX: 
            function(objetB, i) {
                return objetB.getAttribute('data-x') + (350 * i);
            },
        duration: 500,
        complete: bougerObjetsB
    })    
}

// Fait sortir objets 4, 5, 6
function bougerObjetsB() {    
    anime({
        targets: '.objets .objetB',
        translateX: -250,
        rotate: 90,
        duration: 2000,
        easing: 'easeInOutElastic',
        delay: anime.stagger(600, {from: 'last'}),
        complete: repositionnerObjetsA,
    })
}

// Repositionne les objets 1, 2, 3 au dessus de l'écran
function repositionnerObjetsA() {
    anime ({
        targets: '.objets .objetA',
        keyframes: [
            {translateY: -250},
            {translateX: 150}
        ],
        complete: repositionnerObjetsB
    })
}

// Repositionne les objets 4, 5, 6 au dessus de l'écran
function repositionnerObjetsB() {
    anime ({
        targets: '.objets .objetB',
        keyframes: [
            {translateY: -250},
            {translateX: 560}
        ],
        complete: tomberObjetsA
    })
}

// Fait tomber objets 1, 2, 3 dans boîte
function tomberObjetsA() {
    anime({
        targets: '.objets .objetA',
        keyframes: [
            {translateY: 350, scale: .9, rotate: '180deg', easing: 'spring(1, 80, 50, 0)'},
            {scale: 0}
        ],
        delay: anime.stagger(200, {start: 2000}),
        complete: tomberObjetsB
    })
}

// Fait tomber objets 4, 5, 6 dans boîte
function tomberObjetsB() {
    anime({
        targets: '.objets .objetB',
        keyframes: [
            {translateY: 350, scale: .9, rotate: '180deg', easing: 'spring(1, 80, 50, 0)'},
            {scale: 0}
        ],
        delay: anime.stagger(200)
    })
}

