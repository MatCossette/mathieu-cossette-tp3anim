TP3 Techniques d'animation

Mathieu Cossette 
Marie-Jeanne Vadnais
Antony Lacerte

Scénario:

plan 1 (3s)

Arc-en-ciel trace un chemin (gauche à droite) en ramassant des objets aléatoires.
Les objets sont en noir et blanc et se colorent lorsqu'ils sont touchés par l'arc-en-ciel.

plan 2 (3s)

Arc-en-ciel trace un chemin (droite à gauche)en ramassant des objets aléatoires.
Les objets sont en noir et blanc et se colorent lorsqu'ils sont touchés par l'arc-en-ciel.

plan 3 (5s)

Personnage tient une boîte vers laquelle tous les rayons de l'arc-en-ciel convergent. 
Les objets arrivent dans la boîte. 
Le personnage bouge la boîte pour encaisser tous les objets.

plan 4 (4s)

Le personnage souris.
La boîte se referme. 
Le personnage s'efface.

plan 5 (3s)

Texte s'affiche à l'écran (prix, spécial, etc).